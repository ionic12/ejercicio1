import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { FormsModule } from '@angular/forms';
import { AccessProviders } from './providers/access-providers';

import { AppComponent } from './app.component';
import { TiendasComponent } from './tiendas/tiendas.component';
import { PosadasComponent } from './posadas/posadas.component';
import { ComidaComponent } from './comida/comida.component';
import { MedicoComponent } from './medico/medico.component';
import { TallerComponent } from './taller/taller.component';
import { FlorafaunaComponent } from './florafauna/florafauna.component';
import { FfDetailComponent } from './florafauna/ff-detail/ff-detail.component';
import { AboutComponent } from './about/about.component';
import { UpdateComponent } from './update/update.component';
import { AppRoutingModule } from './app-routing.module';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { HttpClientModule } from '@angular/common/http';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { PopInfoRutaComponent } from './pop-info-ruta/pop-info-ruta.component';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    AppComponent, 
    TiendasComponent,
    PosadasComponent,
    ComidaComponent,
    MedicoComponent,
    TallerComponent,
    FlorafaunaComponent,
    FfDetailComponent,
    AboutComponent,
    UpdateComponent,
    PopInfoRutaComponent
  ],
  exports: [
    PopInfoRutaComponent
  ],
  entryComponents: [],
  imports: [
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonicStorageModule.forRoot()

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    CallNumber, // Llamada telefónica
    SQLite,
    SQLitePorter,
    AccessProviders,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
