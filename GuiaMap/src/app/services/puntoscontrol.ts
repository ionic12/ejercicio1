export class PuntoControl {
    id_punto_control: string;
    latitud: string;
    longitud: string;
    direccion: string;
    altura: string;
    created: string;
    updated: string;
}
