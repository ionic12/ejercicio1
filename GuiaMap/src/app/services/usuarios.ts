export class Usuarios {
    id_usuario: number;
    nombre: string;
    apellidos: string;
    correo: string;
    contrasenia: string;
    sexo: string;
    ultima_fecha_ingreso: string;
    clave: string;
    created_at: string;
    updated_at:string;
}
