import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Contacto } from './contacto'; //Clase contacto
import { Ruta } from './ruta'; //Clase ruta
import { PuntoControl } from './puntoscontrol'; //Clase puntos control
import { Servicios } from './servicios'; //Clase Servicios
import {Planta} from './../florafauna/florafauna.model';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})

export class DbService {
  private storage: SQLiteObject;
  contactosList = new BehaviorSubject([]);
  tiendasList = new BehaviorSubject([]);
  posadasList = new BehaviorSubject([]);
  comidaList = new BehaviorSubject([]);
  medicosList = new BehaviorSubject([]);
  talleresList = new BehaviorSubject([]);
  rutasList = new BehaviorSubject([]);
  informacionList = new BehaviorSubject([]);
  puntosControlList = new BehaviorSubject([]);
  private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform: Platform,
    private sqlite: SQLite,
    private httpClient: HttpClient,
    private sqlPorter: SQLitePorter,
  ) {
    this.platform.ready().then(() => {
      this.sqlite.create({
        name: 'physis_movil.db',
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          this.storage = db;
          this.getDatosBD(); //Obtengo todos los datos de la base de datos
        });
    });
  }

  dbState() {
    return this.isDbReady.asObservable();
  }

  fetchContactos(): Observable<Contacto[]> {
    return this.contactosList.asObservable();
  }

  fetchServicios(pTipo): Observable<Servicios[]> {
    if(pTipo === 'T'){ 
      return this.tiendasList.asObservable();
    }else if(pTipo === 'H'){ 
      return this.posadasList.asObservable();
    }else if(pTipo === 'C'){ 
      return this.comidaList.asObservable();
    }else if(pTipo === 'M'){ 
      return this.medicosList.asObservable();
    }else if(pTipo === 'R'){ 
      return this.talleresList.asObservable();
    }
  }

  fetchInformacion(): Observable<Planta[]> {
    return this.informacionList.asObservable();
  }

  fetchRutas(): Observable<Ruta[]> {
    return this.rutasList.asObservable();
  }

  fetchPuntosControl(): Observable<PuntoControl[]> {
    return this.puntosControlList.asObservable();
  }

  // Render datos 
  getDatosBD() {
    this.httpClient.get(
      'assets/physis_movil.sql',
      { responseType: 'text' }
    ).subscribe(data => {
      this.sqlPorter.importSqlToDb(this.storage, data)
        .then(_ => {
          this.getPuntosControl();  // Obtengo los datos de puntos de control
          this.getRutas();          // Obtengo los datos de rutas
          this.getContactos();      // Obtengo los datos de contacto
          this.getServicios();      // Obtengo los datos de servicios
          this.getInformacion();    // Obtengo los datos de informacion
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

  // Obtiene datos de contacto
  getContactos() {
    return this.storage.executeSql('SELECT celular, facebook, instagram FROM contacto WHERE id_contacto = 1', []).then(res => {
      let items: Contacto[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) {
          items.push({
            celular: res.rows.item(i).celular,
            facebook: res.rows.item(i).facebook,
            instagram: res.rows.item(i).instagram
          });
        }
        console.log("TOTAL CONTACTOS: " + res.rows.length);
      }
      this.contactosList.next(items);
    });
  }

  // Obtiene datos de servicios
  getServicios() {
    this.getServiciosXTipo('T'); //Tiendas
    this.getServiciosXTipo('H'); //Posadas
    this.getServiciosXTipo('C'); //Comida
    this.getServiciosXTipo('M'); //Médico
    this.getServiciosXTipo('R'); //Talleres
  }

  getServiciosXTipo(pTipo){
    return this.storage.executeSql('SELECT descripcion, tipo, latitud, longitud, contacto_nombre, contacto_telefono, horario, created_at, updated_at  FROM servicios WHERE tipo = ?', [pTipo]).then(res => {
      let items2: Servicios[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) {
          items2.push({
            descripcion: res.rows.item(i).descripcion,
            tipo: res.rows.item(i).tipo,
            latitud: res.rows.item(i).latitud,
            longitud: res.rows.item(i).longitud,
            contactonombre: res.rows.item(i).contacto_nombre,
            contactotelefono: res.rows.item(i).contacto_telefono,
            horario: res.rows.item(i).horario,
            createdat: res.rows.item(i).created_at,
            updatedat: res.rows.item(i).updated_at,
          });
        }
      }
      if(pTipo === 'T'){ 
        console.log("TOTAL TIENDAS: " + res.rows.length);
        this.tiendasList.next(items2);
      }else if(pTipo === 'H'){ 
        console.log("TOTAL POSADAS: " + res.rows.length);
        this.posadasList.next(items2);
      }else if(pTipo === 'C'){ 
        console.log("TOTAL COMIDA: " + res.rows.length);
        this.comidaList.next(items2);
      }else if(pTipo === 'M'){ 
        console.log("TOTAL MEDICOS: " + res.rows.length);
        this.medicosList.next(items2);
      }else if(pTipo === 'R'){ 
        console.log("TOTAL TALLERES: " + res.rows.length);
        this.talleresList.next(items2);
      }
    });
  }

  // Obtiene datos de rutas
  getRutas() {
    return this.storage.executeSql('SELECT id_ruta, descripcion, dificultad, distancia FROM rutas', []).then(res => {
      let items: Ruta[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) {
          items.push({
            id_ruta: res.rows.item(i).id_ruta,
            descripcion: res.rows.item(i).descripcion,
            dificultad: res.rows.item(i).dificultad,
            distancia: res.rows.item(i).distancia
          });
        }
        console.log("TOTAL RUTAS: " + res.rows.length);
      }
      this.rutasList.next(items);
    });
  }

  // Obtiene datos de puntos de control
  getPuntosControl(){
    return this.storage.executeSql('SELECT latitud, longitud, direccion, altura, created_at, updated_at,id_punto_control FROM puntos_control', []).then(res => {
      let items: PuntoControl[] = [];
      if (res.rows.length > 0) {
        for (var i = 0; i < res.rows.length; i++) { 
          items.push({ 
            latitud: res.rows.item(i).latitud,  
            longitud: res.rows.item(i).longitud,
            direccion: res.rows.item(i).direccion,
            altura: res.rows.item(i).altura,
            created: res.rows.item(i).created_at,
            updated: res.rows.item(i).updated_at,
            id_punto_control: res.rows.item(i).id_punto_control
          });
        }
        console.log("TOTAL PUNTOS DE CONTROL: " + res.rows.length);
      }
      this.puntosControlList.next(items);
    });
  }

  // Render datos Informacion
  getDatosInformacion() {
    this.httpClient.get(
      'assets/physis_movil.sql', 
      {responseType: 'text'}
    ).subscribe(data2 => {
      this.sqlPorter.importSqlToDb(this.storage, data2)
        .then(_ => {
          this.getInformacion();
          this.isDbReady.next(true);
        })
        .catch(error => console.error(error));
    });
  }

// Get list
getInformacion(){
  return this.storage.executeSql('SELECT * FROM informacion', []).then(res => {
    let items3: Planta[] = [];
    if (res.rows.length > 0) {
      for (var i = 0; i < res.rows.length; i++) { 
          items3.push({ 
          idInformacion: res.rows.item(i).id_informacion,
          descripcion: res.rows.item(i).descripcion,
          tipo: res.rows.item(i).tipo,
          rutaImagen: res.rows.item(i).ruta_imagen,
         });
      }
      console.log("Informacion: "+i);
    }
    this.informacionList.next(items3);
    
  });
}

}