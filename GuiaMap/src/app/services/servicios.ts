export class Servicios {
    descripcion: string;
    tipo: string;
    latitud: string;
    longitud: string;
    contactonombre: string;
    contactotelefono: string;
    horario: string;
    createdat: string;
    updatedat: string;
}
