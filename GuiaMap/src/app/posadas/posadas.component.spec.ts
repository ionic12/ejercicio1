import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PosadasComponent } from './posadas.component';

describe('PosadasComponent', () => {
  let component: PosadasComponent;
  let fixture: ComponentFixture<PosadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosadasComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PosadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
