import { Component, OnInit, Input } from '@angular/core';
import { DbService } from './../services/db.service';

@Component({
  selector: 'app-pop-info-ruta',
  templateUrl: './pop-info-ruta.component.html',
  styleUrls: ['./pop-info-ruta.component.scss'],
})
export class PopInfoRutaComponent implements OnInit {
  DataRutas: any[] = []
  idRuta: number = 2; //Este valor es dado por el componente padre que lo llama

  constructor(
    private db: DbService
  ) { }

  ngOnInit() {
    this.db.dbState().subscribe((res) => { //Se usa para leer la información de contacto de la base de datos
      if(res){
        this.db.fetchRutas().subscribe(item => {
          this.DataRutas = item
        })
      }
    });
  }

  

}
