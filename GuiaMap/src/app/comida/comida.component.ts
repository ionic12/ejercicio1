import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';
import { DbService } from './../services/db.service';
import { CallNumber } from '@ionic-native/call-number/ngx'; //Para realizar llamadas teléfonicas
import { Servicios } from '../services/servicios';

declare var google;

interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string; //Es la descripción
  telefono: string;
  horario: string;
}

@Component({
  selector: 'app-comida',
  templateUrl: './comida.component.html',
  styleUrls: ['./comida.component.scss'],
})
export class ComidaComponent implements OnInit {
  map: null;

  DataContactos: any[] = [];
  dataServicios: Servicios[] = [];

  //Creación de Arreglo de Marcadores
  markers: Marker[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private geolocation: Geolocation,
    private loadCtrl: LoadingController,
    private db: DbService,
    public callNumber: CallNumber,
  ) { }

  ngOnInit() {
    this.db.dbState().subscribe((res) => { // Se usa para leer la información de la base de datos
      if (res) {
        this.db.fetchContactos().subscribe(item => {
          console.log("ITEM: " + item.length);
          this.DataContactos = item;
        });
        this.db.fetchServicios('C').subscribe(item2 => {
          console.log("ITEM2: " + item2.length);
          this.dataServicios = item2;
        });
      }
      this.fillMarkers();
      this.loadMap(); // Cargo el mapa
    });
  }

  async loadMap() {
    console.log("Hola desde loadMap Comida");
    const loading = await this.loadCtrl.create();
    loading.present();
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('comid');
    //const rta = await this.geolocation.getCurrentPosition();
    // create LatLng object
    const myLatLng = { lat: 19.638634, lng: -97.098923 }; //Lo ubico al centro de Las Vigas, Ver.
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 13.5,
      //Variable para cargar el tipo de mapa
      mapTypeId: 'hybrid',
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      loading.dismiss();
      this.renderMarkers();
      mapEle.classList.add('show-map');
    });
  }

  renderMarkers() {
    this.markers.forEach(marker => { //Por cada registro que se encuentre en el arreglo
      const contentString =  //Genero su ventana de información
        '<div id="content">' +
        '<div id="siteNotice">' +
        "</div>" +
        '<div id="bodyContent">' +
        "<p><b>" + marker.title + "          </b></p>" +
        "<p><b>Teléfono: </b>" + marker.telefono + "          " +
        "<br><b>Horario: </b>" +
        "<br>" + marker.horario + "          </p>" +
        "</div>" +
        "</div>";
      const infoWindow = new google.maps.InfoWindow({
        content: contentString
      });
      const m = this.addMarker(marker);   //Agrego el punto del servicio al mapa
      m.addListener("click", () => {      //Cuando se de click en el punto
        infoWindow.open(this.map, m);     //Se despliega la ventana de información
      });
    });
  }

  fillMarkers() {
    for (var i = 0; i < this.dataServicios.length; i++) {
      console.log(this.dataServicios[i].descripcion);
      let mark: Marker = { position: { lat: 0, lng: 0 }, title: '', telefono: '', horario: '' };
      mark.position.lat = Number(this.dataServicios[i].latitud);
      mark.position.lng = Number(this.dataServicios[i].longitud);
      mark.title = this.dataServicios[i].descripcion;
      if(this.dataServicios[i].contactotelefono!=null){ mark.telefono = this.dataServicios[i].contactotelefono; }else{ mark.telefono =""; }
      if(this.dataServicios[i].horario!=null){ mark.horario = this.dataServicios[i].horario; }else{ mark.horario =""; }
      this.markers.push(mark);
    }
  }

  addMarker(marker: Marker) {
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      //Animación de Marker
      draggable: false,
      animation: google.maps.Animation.DROP,
      title: marker.title,
      Opacity: 0.75,
      dblClick: marker.title
    });
  }

  //Función que se usa para realizar llamada telefónica desde el botón de "Contacto"
  llamar(celular) {
    const alert = document.createElement('ion-alert');
    alert.cssClass = 'my-custom-class';
    alert.message = 'Llamar ' + celular;
    alert.buttons = [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Llamar',
        handler: () => {
          console.log('Confirm Ok');
          this.callNumber.callNumber(celular, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
        }
      }
    ];

    document.body.appendChild(alert);
    return alert.present();
  }

  //Función que abre url en ventana nueva, se usa para abrir facebook e instagram
  abrirVentana(url) {
    window.open(url, '_system', 'location=yes');
  }
}
