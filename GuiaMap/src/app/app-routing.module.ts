import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { TiendasComponent } from './tiendas/tiendas.component';
import { PosadasComponent } from './posadas/posadas.component';
import { ComidaComponent } from './comida/comida.component';
import { MedicoComponent } from './medico/medico.component';
import { TallerComponent } from './taller/taller.component';
import { FlorafaunaComponent } from './florafauna/florafauna.component';
import { FfDetailComponent } from './florafauna/ff-detail/ff-detail.component';
import { AboutComponent } from './about/about.component';
import { UpdateComponent } from './update/update.component';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'tiendas', component: TiendasComponent },
  { path: 'posadas', component: PosadasComponent },
  { path: 'comida', component: ComidaComponent },
  { path: 'medico', component: MedicoComponent },
  { path: 'taller', component: TallerComponent },
  { path: 'florafauna',
    children:[
      {
        path: '', component: FlorafaunaComponent  
      },
      {
        path: ':ffid', component: FfDetailComponent  
      }
    ]
  },
  { path: 'about', component: AboutComponent },
  { path: 'update', component: UpdateComponent },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'inicio',
    loadChildren: () => import('./inicio/inicio.module').then( m => m.InicioPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
