import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx'; //Para realizar llamadas teléfonicas
import { MenuController } from '@ionic/angular';
import { FormBuilder } from "@angular/forms";
import { DbService } from './../services/db.service';
import { ToastController } from '@ionic/angular';
import { Router } from "@angular/router";
import { PopoverController } from '@ionic/angular';
import { PopInfoRutaComponent } from '../pop-info-ruta/pop-info-ruta.component';
import { computeStackId } from '@ionic/angular/directives/navigation/stack-utils';

import {  AlertController,NavController } from '@ionic/angular';
import { AccessProviders } from '../providers/access-providers'
import { Storage } from '@ionic/storage';


declare var google;

interface Marker {
  position: {
    lat: number,
    lng: number,
  };
  title: string,
  label: string
}

interface Position {
  lat: number,
  lng: number,
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [
    CallNumber,
    PopInfoRutaComponent
  ]
})
export class HomePage implements OnInit {
  
  datastorage:any;
  name:string;
  map: null;

  //Arreglos que se usan para almacenar los datos de la base de datos
  DataContactos: any[] = [];
  DataPuntosControl: any[] = [];

  markers: Marker[] = []; //Creación de Arreglo de Marcadores, guarda los puntos de control

  //Preparado para soportar hasta 10 rutas
  //puntos_Ruta1: Position[] = []; 
  ruta1: any; //Creación de Arreglo de puntos de control de Ruta
  //puntos_Ruta2: Position[] = []; 
  ruta2: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta3: Position[] = []; ruta3: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta4: Position[] = []; ruta4: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta5: Position[] = []; ruta5: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta6: Position[] = []; ruta6: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta7: Position[] = []; ruta7: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta8: Position[] = []; ruta8: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta9: Position[] = []; ruta9: any; //Creación de Arreglo de puntos de control de Ruta
  puntos_Ruta10: Position[] = []; ruta10: any; //Creación de Arreglo de puntos de control de Ruta

  constructor(
    private activatedRoute: ActivatedRoute,
    private geolocation: Geolocation,
    private loadCtrl: LoadingController,
    public callNumber: CallNumber,
    private menu: MenuController,
    private db: DbService,
    public formBuilder: FormBuilder,
    private toast: ToastController,
    private router: Router,
    private popoverCtrl: PopoverController,
    
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public navCtrl: NavController,
    private popInfoRuta: PopInfoRutaComponent
  ) { }

  ngOnInit() {
    this.db.dbState().subscribe((res) => { //Se usa para leer la información de contacto de la base de datos
      if (res) {
        this.db.fetchContactos().subscribe(item => {
          this.DataContactos = item
        });
        this.db.fetchPuntosControl().subscribe(item2 => {
          this.DataPuntosControl = item2
        });
        //this.fillPuntosControl(); //Lleno el arreglo con los puntos de control de la base de datos
        //console.log(this.DataPuntosControl);
        //console.log(this.markers);
        this.loadMap(); //Cargo el mapa
      }
    });
  }

  ionViewDidEnter(){
    this.storage.get('storage_xxx').then((res)=>{console.log(res);
      this.datastorage = res;
      this.name = this.datastorage.nombre;
    
    });
  }

  async prosesLogout(){
    this.storage.clear();
    this.navCtrl.navigateRoot(['/login']);
    const toast = await this.toastCtrl.create({
      message:'Logueado Correctamente',
      duration: 1500,
      
    });
    toast.present();
  }
  

  async loadMap() {
    console.log("Hola desde loadMap");
    const loading = await this.loadCtrl.create();
    loading.present();
    // create a new map by passing HTMLElement
    const mapEle: HTMLElement = document.getElementById('map');
    //const rta = await this.geolocation.getCurrentPosition();
    // create LatLng object
    const myLatLng = { lat: 19.601725, lng: -97.116530 };
    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 13.5,
      //Variable para cargar el tipo de mapa
      mapTypeId: 'hybrid',
    });

    // creacion de puntos en un arreglo rojo
    var puntos_Ruta1 = [
      { lat: 19.621335, lng: -97.105583 },//	10	SALIDA
      { lat: 19.622216, lng: -97.105751 },//	11	
      { lat: 19.623521, lng: -97.105374 },//	12	
      { lat: 19.624007, lng: -97.105544 },//	13	
      { lat: 19.624664, lng: -97.105386 },//	14	
      { lat: 19.624459, lng: -97.105938 },//	15	
      { lat: 19.624833, lng: -97.105949 },//	16	
      { lat: 19.625552, lng: -97.105996 },//	17	
      { lat: 19.626022, lng: -97.105719 },//	18	
      { lat: 19.626251, lng: -97.10547 },	//	19	
      { lat: 19.626673, lng: -97.105287 },//	20	
      { lat: 19.62697, lng: -97.105127 }, //	21	
      { lat: 19.627097, lng: -97.105242 },//	22	
      { lat: 19.627103, lng: -97.105321 },//	23	
      { lat: 19.626983, lng: -97.105397 },//	24	
      { lat: 19.626712, lng: -97.105605 },//	25	
      { lat: 19.626528, lng: -97.105714 },//	26	
      { lat: 19.626405, lng: -97.106152 },//	27	
      { lat: 19.626473, lng: -97.106348 },//	28	I
      { lat: 19.626395, lng: -97.106517 },//	29	D
      { lat: 19.626845, lng: -97.107081 },//	30	I
      { lat: 19.627078, lng: -97.107481 },//	31	D
      { lat: 19.62735, lng: -97.10782 },	//	32	D
      { lat: 19.627903, lng: -97.108318 },//	33	D
      { lat: 19.628784, lng: -97.108612 },//	34	D
      { lat: 19.6293, lng: -97.108789 },	//	35	I
      { lat: 19.629643, lng: -97.109549 },//	36	D
      { lat: 19.629705, lng: -97.109802 },//	37	D!
      { lat: 19.629659, lng: -97.110334 },//	38	D!
      { lat: 19.629686, lng: -97.110911 },//	39	I
      { lat: 19.629651, lng: -97.110959 },//	40	D
      { lat: 19.629615, lng: -97.111132 },//	41	I
      { lat: 19.629671, lng: -97.111969 },//	42	
      { lat: 19.629496, lng: -97.112304 },//	43	D
      { lat: 19.627536, lng: -97.114132 },//	44	D
      { lat: 19.627286, lng: -97.114356 },//	45	
      { lat: 19.625998, lng: -97.115104 },//	46	
      { lat: 19.624931, lng: -97.115044 },//	47	
      { lat: 19.624488, lng: -97.115608 },//	48	
      { lat: 19.623943, lng: -97.116555 },//	49	
      { lat: 19.623243, lng: -97.119852 },//	50	
      { lat: 19.62277, lng: -97.120174 },	//	51	
      { lat: 19.622511, lng: -97.120188 },//	52	
      { lat: 19.621418, lng: -97.121651 },//	53	
      { lat: 19.620226, lng: -97.122408 },//	54	
      { lat: 19.619452, lng: -97.123079 },//	55	
      { lat: 19.616514, lng: -97.127129 },//	56	
      { lat: 19.610774, lng: -97.131831 },//	57	
      { lat: 19.608135, lng: -97.130637 },//	58	
      { lat: 19.60814, lng: -97.130617 },	//	59	
      { lat: 19.608269, lng: -97.12997 },	//	60	
      { lat: 19.608267, lng: -97.129968 },//	61	
      { lat: 19.604828, lng: -97.129637 },//	62	
      { lat: 19.603564, lng: -97.129902 },//	63	
      { lat: 19.601818, lng: -97.130651 },//	64	
      { lat: 19.60021, lng: -97.131062 },	//	65	
      { lat: 19.598885, lng: -97.131524 },//	66	
      { lat: 19.597676, lng: -97.131989 },//	67	
      { lat: 19.596199, lng: -97.132686 },//	68	
      { lat: 19.595851, lng: -97.133635 },//	69	
      { lat: 19.595398, lng: -97.133756 },//	70	
      { lat: 19.59248, lng: -97.135045 },	//	71	
      { lat: 19.592232, lng: -97.135104 },//	72	
      { lat: 19.591933, lng: -97.13541 },	//	73	
      { lat: 19.589276, lng: -97.134074 },//	74	
      { lat: 19.589139, lng: -97.133744 },//	75	
      { lat: 19.589394, lng: -97.132897 },//	76	
      { lat: 19.589782, lng: -97.132491 },//	77	
      { lat: 19.590275, lng: -97.131842 },//	78	
      { lat: 19.588014, lng: -97.131595 },//	79	
      { lat: 19.587645, lng: -97.131449 },//	80	
      { lat: 19.586931, lng: -97.131116 },//	81	
      { lat: 19.585458, lng: -97.131527 },//	82	
      { lat: 19.584546, lng: -97.131618 },//	83	
      { lat: 19.583369, lng: -97.131952 },//	84	
      { lat: 19.582845, lng: -97.132272 },//	85	
      { lat: 19.581626, lng: -97.131953 },//	86	
      { lat: 19.579282, lng: -97.131398 },//	87	
      { lat: 19.57866, lng: -97.131384 },	//	88	
      { lat: 19.577746, lng: -97.130855 },//	89	
      { lat: 19.577088, lng: -97.130892 },//	90	
      { lat: 19.575955, lng: -97.130785 },//	91	
      { lat: 19.575332, lng: -97.129945 },//	92	
      { lat: 19.573165, lng: -97.127102 },//	93	
      { lat: 19.57197, lng: -97.126049 },	//	94	
      { lat: 19.570586, lng: -97.123731 },//	95	
      { lat: 19.570628, lng: -97.121772 },//	96	
      { lat: 19.570677, lng: -97.120113 },//	97	
      { lat: 19.569683, lng: -97.119278 },//	98	
      { lat: 19.570385, lng: -97.118571 },//	99	
      { lat: 19.570855, lng: -97.117494 }//	100	
    ];

    // creacion de puntos en un arreglo verde
    var puntos_Ruta2 = [
      { lat: 19.570855, lng: -97.117494 },//	100	
      { lat: 19.574347, lng: -97.108469 },//	101	
      { lat: 19.573799, lng: -97.106947 },//	102	
      { lat: 19.572653, lng: -97.103684 },//	103	
      { lat: 19.57173, lng: -97.098533 },	//	104	
      { lat: 19.573638, lng: -97.099559 },//	105	
      { lat: 19.573929, lng: -97.098797 },//	106	
      { lat: 19.573359, lng: -97.097576 },//	107	
      { lat: 19.573733, lng: -97.097136 },//	108	
      { lat: 19.578225, lng: -97.099951 },//	109	
      { lat: 19.57898, lng: -97.09839 },	//	110	
      { lat: 19.579602, lng: -97.098652 },//	111	
      { lat: 19.58194, lng: -97.100774 },	//	112	
      { lat: 19.584739, lng: -97.102625 },//	113	
      { lat: 19.587537, lng: -97.100853 },//	114	
      { lat: 19.587709, lng: -97.100889 },//	115	
      { lat: 19.589626, lng: -97.102272 },//	116	
      { lat: 19.588812, lng: -97.104978 },//	117	
      { lat: 19.594158, lng: -97.110779 },//	118	
      { lat: 19.594277, lng: -97.112797 },//	119	
      { lat: 19.594628, lng: -97.113753 },//	120	
      { lat: 19.595043, lng: -97.113987 },//	121	
      { lat: 19.596187, lng: -97.114953 },//	122	
      { lat: 19.59907, lng: -97.112923 },	//	123	
      { lat: 19.601362, lng: -97.113357 },//	124	
      { lat: 19.602555, lng: -97.112572 },//	125	
      { lat: 19.602705, lng: -97.113129 },//	126	
      { lat: 19.603847, lng: -97.113864 },//	127	
      { lat: 19.60518, lng: -97.114425 },	//	128	
      { lat: 19.608166, lng: -97.115378 },//	129	
      { lat: 19.609579, lng: -97.115269 },//	130	
      { lat: 19.61287, lng: -97.114655 },	//	131	
      { lat: 19.613247, lng: -97.114617 },//	132	
      { lat: 19.613498, lng: -97.11434 },	//	133	
      { lat: 19.613385, lng: -97.113724 },//	134	
      { lat: 19.61326, lng: -97.11313 },	//	135	
      { lat: 19.615124, lng: -97.111266 },//	136	
      { lat: 19.615161, lng: -97.111166 },//	137	
      { lat: 19.615224, lng: -97.110854 },//	138	
      { lat: 19.61493, lng: -97.110289 },	//	139	
      { lat: 19.614152, lng: -97.109931 },//	140	
      { lat: 19.614081, lng: -97.109731 },//	141	
      { lat: 19.615685, lng: -97.109323 },//	142	
      { lat: 19.61663, lng: -97.109054 },	//	143	
      { lat: 19.617718, lng: -97.109185 },//	144	
      { lat: 19.620279, lng: -97.108745 },//	145	
      { lat: 19.621886, lng: -97.106936 }	//	146	
    ];
    
    this.dibujarRuta(puntos_Ruta1, this.ruta1, '#FF0000');
    this.dibujarRuta(puntos_Ruta2, this.ruta2, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta3, this.ruta3, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta4, this.ruta4, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta5, this.ruta5, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta6, this.ruta6, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta7, this.ruta7, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta8, this.ruta8, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta9, this.ruta9, '#64FF00');
    this.dibujarRuta(this.puntos_Ruta10, this.ruta10, '#64FF00');

    this.fillPuntosControl(); //Lleno el arreglo con los puntos de control de la base de datos
    console.log(this.DataPuntosControl);
    console.log(this.markers);

    //se dibujan los puntos de control en el mapa
    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      loading.dismiss();
      this.renderMarkers();
      mapEle.classList.add('show-map');

    });

    console.log("Fin de loadMapa");

  }

  dibujarRuta(puntos_ruta, ruta, color) {
    //Lleno el arreglo con los puntos de la base de datos
    //puntos_ruta

    if (puntos_ruta.length > 0) { //Solo si hay puntos de control definidos para las rutas
      console.log("Dibujando ruta");
      //Creación de Polilinea
      var ruta = new google.maps.Polyline({
        path: puntos_ruta,
        geodesic: true,
        strokeColor: color,
        strokeOpacity: 1.0,
        strokeWeight: 4
      });

      //Asignacion de la Polilinea al Mapa
      ruta.setMap(this.map);

      //Evento para que puedan dar click en la ruta y desplegar el popover de la información
      google.maps.event.addListener(ruta, 'click', () => {
        this.mostrarPopRuta();
      });
    }

  }

  //Muestra popover para desplegar información de la ruta
  async mostrarPopRuta() {
    const popover = await this.popoverCtrl.create({
      component: PopInfoRutaComponent,
      mode: 'ios',
    });

    return await popover.present();
  }

  renderMarkers() {
    this.markers.forEach(marker => { //Por cada registro que se encuentre en el arreglo
      const contentString =  //Genero su ventana de información
        '<div id="content">' +
        '<div id="siteNotice">' +
        "</div>" +
        '<h1 id="firstHeading" class="firstHeading">Punto de control</h1>' +
        '<div id="bodyContent">' +
        "<p><b>Latitud: </b>" + marker.position.lat +
        "<br><b>Longitud: </b>" + marker.position.lng + "</p>" +
        "</div>" +
        "</div>";
      const infoWindow = new google.maps.InfoWindow({
        content: contentString
      });
      const m = this.addMarker(marker);   //Agrego el punto del servicio al mapa
      m.addListener("click", () => {      //Cuando se de click en el punto
        infoWindow.open(this.map, m);     //Se despliega la ventana de información
      });
    });
  }

  addMarker(marker: Marker) {
    return new google.maps.Marker({
      position: marker.position,
      map: this.map,
      //Animación de Marker
      draggable: false,
      animation: google.maps.Animation.DROP,
      title: marker.title,
      Opacity: 0.75,
      label: marker.label
    });
  }

  //Función que se usa para realizar llamada telefónica desde el botón de "Contacto"
  llamar(celular) {
    const alert = document.createElement('ion-alert');
    alert.cssClass = 'my-custom-class';
    alert.message = 'Llamar ' + celular;
    alert.buttons = [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel');
        }
      }, {
        text: 'Llamar',
        handler: () => {
          console.log('Confirm Ok');
          this.callNumber.callNumber(celular, true)
            .then(res => console.log('Launched dialer!', res))
            .catch(err => console.log('Error launching dialer', err));
        }
      }
    ];

    document.body.appendChild(alert);
    return alert.present();
  }

  //Función que abre url en ventana nueva, se usa para abrir facebook e instagram
  abrirVentana(url) {
    window.open(url, '_system', 'location=yes');
  }

  //Función que llena arreglo con los puntos de control de la base de datos
  fillPuntosControl() {
    console.log("Llenando puntos de control");
    for(var i=0; i < this.DataPuntosControl.length; i++){ 
      let mark: Marker = { position: { lat: 0, lng: 0 }, title: '', label: '' };
      mark.position.lat= Number(this.DataPuntosControl[i].latitud);
      mark.position.lng= Number(this.DataPuntosControl[i].longitud);
      mark.title= this.DataPuntosControl[i].descripcion;
      mark.label = String(this.DataPuntosControl[i].id_punto_control);
      this.markers.push(mark);
    }
  }



}
