import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { FlorafaunaService } from '../florafauna.service';
import { Planta } from '../florafauna.model';

@Component({
  selector: 'app-ff-detail',
  templateUrl: './ff-detail.component.html',
  styleUrls: ['./ff-detail.component.scss'],
})
export class FfDetailComponent implements OnInit {

  planta: Planta;

  constructor(private activatedRoute: ActivatedRoute, private florafaunaService: FlorafaunaService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap =>{
      const recipeId=paramMap.get('ffid');
      this.planta=this.florafaunaService.getPlanta(recipeId);
      console.log(this.planta);

    })
  }

}
