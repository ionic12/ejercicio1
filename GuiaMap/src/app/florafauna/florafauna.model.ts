export interface Planta{
    idInformacion: string;
    descripcion: string;
    tipo: string;
    rutaImagen: string;
}