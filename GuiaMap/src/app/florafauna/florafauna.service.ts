import { Injectable } from '@angular/core';
import {Planta} from './florafauna.model';
import { DbService } from './../services/db.service';

@Injectable({
  providedIn: 'root'
})
export class FlorafaunaService {


  private plantas: Planta[]  = [ {
    idInformacion: '1',
    descripcion: 'Rosa',
    rutaImagen: 'https://www.ofrendavirgendelpilar.com/wp-content/uploads/2018/09/rosa-roja-ofrenda.jpg',
    tipo: ''
    }
  ];
  private plant: Planta={
    idInformacion: '1',
    descripcion: 'Rosa',
    rutaImagen: 'https://www.ofrendavirgendelpilar.com/wp-content/uploads/2018/09/rosa-roja-ofrenda.jpg',
    tipo: ''
    };
  constructor(
    private db: DbService
    ) { }

  getFlora(){
    this.db.dbState().subscribe((res) => { // Se usa para leer la información de la base de datos
      if(res){
        this.db.fetchInformacion().subscribe(item => {
          console.log("ITEM: "+item.length);
          this.plantas = item;
        });
      }
    });   
   
    return[...this.plantas]
  }

  getPlanta(plantaID: string){
    

    return{
    ...this.plantas.find(planta=>{
        return ((planta.idInformacion+'') === (plantaID));
      })
    }
  }
  getFauna(){
    
  }
}
