import { Component, OnInit, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlorafaunaService } from './florafauna.service';

@Component({
  selector: 'app-florafauna',
  templateUrl: './florafauna.component.html',
  styleUrls: ['./florafauna.component.scss'],
})
export class FlorafaunaComponent implements OnInit {

  plantas = []
  
  constructor(private florafaunaService: FlorafaunaService) { }

  ngOnInit() {
    this.plantas = this.florafaunaService.getFlora();
    
  }


}
