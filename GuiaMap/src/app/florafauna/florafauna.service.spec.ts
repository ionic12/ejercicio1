import { TestBed } from '@angular/core/testing';

import { FlorafaunaService } from './florafauna.service';

describe('FlorafaunaService', () => {
  let service: FlorafaunaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlorafaunaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
