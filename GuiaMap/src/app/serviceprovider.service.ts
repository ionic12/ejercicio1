import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {map} from "rxjs/operators";
//import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import {ToastController,LoadingController} from '@ionic/angular';
import {Router} from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class ServiceproviderService {
  registerurl="http://localhost/tutorial3/registration.php";
  loginurl = "http://localhost/tutorial3/login.php";
  result:any;
  responseData:any; 
  constructor(public http: HttpClient, public toastController: ToastController, public loadingController: LoadingController, 
    private route:Router) { }

    registerData(email:string, password:string, cpassword:string){

      return this.http.get(`${this.registerurl}?email=${email}&password=${password}`).pipe(map(results =>{

        this.responseData = results;
        this.route.navigate(['/login']);
      }))
    }
    loginData(email:string, password:string){

      return this.http.get(`${this.loginurl}?${email}&password=${password}`).pipe(map(results => {localStorage.setItem('userdata',JSON.stringify(this.result));
      this.route.navigate(['/home']);
    }))
    }
}
