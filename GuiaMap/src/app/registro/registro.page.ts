import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from "@angular/router";
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import { AccessProviders } from '../providers/access-providers'


@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  nombre: string ="";
  apellidos:string="";
  sexo: string ="";
  correo: string ="";
  contrasenia: string ="";
  confirm_contra: string ="";

  disabledButton;

  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.disabledButton= false;
  }

  async tryRegister(){
    if(this.nombre==""){
      this.presentToast('Tu nombre es requerido');

    }else if (this.apellidos==""){
      this.presentToast('Tus apellidos son requeridos');

    }else if (this.sexo==""){
      this.presentToast('Tu genero es requerido');
  
    }else if (this.correo==""){
      this.presentToast('Tu correo es requerido');
  
    }else if (this.contrasenia==""){
      this.presentToast('Tu contraseña es requerida');
  
    }else if (this.confirm_contra!=this.contrasenia){
      this.presentToast('La contraseña no es la misma');
    
    }else {
      this.disabledButton= true;
      const loader = await this.loadingCtrl.create({
        message: 'Por favor espera',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi:'proses_register',
          nombre: this.nombre,
          apellidos: this.apellidos,
          sexo: this.sexo,
          correo: this.correo,
          contrasenia: this.contrasenia

        } 
        this.accsPrvds.postData(body,'proses_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton=false;
            this.presentToast(res.msg);
            this.router.navigate(['/login']);
          }else{
            loader.dismiss();
            this.disabledButton=false;
            this.presentToast(res.msg);
            
          }
        },(err)=>{
          loader.dismiss();
            this.disabledButton=false;
            this.presentAlert('Timeout');
        });
      });
    }

  }
  
  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message:a,
      duration: 1500,
      position:'top'
    });
    toast.present();
  }

  
    async presentAlert(a){
      const alert = await this.alertCtrl.create({
        header: a,
        backdropDismiss:false,
        buttons:[
          {
            text:'Close',
            handler:(blah)=>{
              console.log('Confirm Cancel:blah');
              //action
            }
          }, {
            text:'Intentalo nuevamente',
            handler:()=>{
              this.tryRegister();
            }

          }
        ]
      });

      await alert.present();
    }

}
