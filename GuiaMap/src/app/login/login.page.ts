import { Component, OnInit } from '@angular/core';

import { Router,ActivatedRoute } from "@angular/router";
import { ToastController, LoadingController, AlertController,NavController } from '@ionic/angular';
import { AccessProviders } from '../providers/access-providers'
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  correo: string ="";
  contrasenia: string ="";
  
  
  disabledButton;
  
  constructor(
    private router: Router,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private accsPrvds: AccessProviders,
    private storage: Storage,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.disabledButton= false;
  }

  async tryLogin(){
    if (this.correo==""){
      this.presentToast('Tu correo es requerido');
  
    }else if (this.contrasenia==""){
      this.presentToast('Tu contraseña es requerida');
  
    
    
    }else {
      this.disabledButton= true;
      const loader = await this.loadingCtrl.create({
        message: 'Por favor espera',
      });
      loader.present();

      return new Promise(resolve => {
        let body = {
          aksi:'proses_login',
          
          correo: this.correo,
          contrasenia: this.contrasenia

        } 
        this.accsPrvds.postData(body,'proses_api.php').subscribe((res:any)=>{
          if(res.success==true){
            loader.dismiss();
            this.disabledButton=false;
            this.presentToast('logueado correctamente');
            this.storage.set('storage_xxx',res.result);
            this.navCtrl.navigateRoot(['/home']);
          }else{
            loader.dismiss();
            this.disabledButton=false;
            this.presentToast('Correo y/o contraseña incorrecta');
            
          }
        },(err)=>{
          loader.dismiss();
            this.disabledButton=false;
            this.presentToast('Timeout');
        });
      });
    }

  }
  
  async presentToast(a){
    const toast = await this.toastCtrl.create({
      message:a,
      duration: 1500,
      
    });
    toast.present();
  }

  abrirRegistro(){
    this.router.navigate(['/registro']);
  }

}
