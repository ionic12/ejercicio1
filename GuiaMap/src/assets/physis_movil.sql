CREATE TABLE IF NOT EXISTS contacto (
	id_contacto	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombreContacto	TEXT,
	celular	TEXT,
	facebook	TEXT,
	instagram	TEXT,
	paginaWeb	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS recorridos (
	id_recorrido	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	id_ruta	INTEGER NOT NULL,
	horaInicio	TEXT,
	horaFin	TEXT,
	id_usuario	INTEGER NOT NULL,
	fecha	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS servicios (
	id_servicio	INTEGER,
	descripcion	TEXT,
	tipo	TEXT,
	latitud	TEXT,
	longitud	TEXT,
	contacto_nombre	TEXT,
	contacto_telefono	TEXT,
	horario	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS puntos_rutas (
	id_ruta	INTEGER NOT NULL,
	id_punto_control	INTEGER NOT NULL,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS rutas (
	id_ruta	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	descripcion	TEXT,
	dificultad	TEXT,
	distancia	REAL,
	color	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS puntos_control (
	id_punto_control	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	latitud	TEXT,
	longitud	TEXT,
	direccion	TEXT,
	altura	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS informacion (
	id_informacion	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	descripcion	TEXT,
	tipo	TEXT,
	ruta_imagen	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
CREATE TABLE IF NOT EXISTS usuarios (
	id_usuario	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	nombre	TEXT,
	apellidos	TEXT,
	correo	TEXT,
	contrasenia	TEXT,
	sexo	TEXT,
	ultima_fecha_ingreso	TEXT,
	clave	TEXT,
	created_at	TEXT,
	updated_at	TEXT
);
INSERT or IGNORE INTO contacto (id_contacto,nombreContacto,celular,facebook,instagram,paginaWeb,created_at,updated_at) VALUES (1,'Luis Fernando López Galicia','2285936641','https://web.facebook.com/PhysisCiclovidaXalapa/','https://www.instagram.com/physisciclovidaxalapa/',NULL,'2020-06-29 12:55:00','2020-06-29 12:55:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (1,'Dr. José Eduardo Ruiz Montiel ','M','19.638766','-97.105187','','282 107 0384','L-V 14:00 a 20:00 y S-D 08:00 a 15:00','2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (2,'IMSS UMF 29 Las Vigas de Ramirez','M','19.638417','-97.103516','','228 156 0441',NULL,'2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (3,'Doctor "Modesto"','M','19.63939','-97.099061','','282 831 4065','L-D 09:00 a 20:00','2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (4,'Doctor Rafael Loozano','M','19.636879','-97.099764','','282 831 4259','L-S 10:30 a 19:00','2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (5,'BICICLETAS IZQUIERDO','R','19.640741','-97.099055',NULL,'2828314181',NULL,'2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (6,'HOTEL EN LAS VIGAS DE RAMIREZ','H','19.6607 ',' -97.0131',NULL,NULL,NULL,'2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (7,'HOTEL MARGARITAS ','H','19.638763','-97.098959',NULL,'22828314137',NULL,'2020-06-30 12:34:00','2020-06-30 12:34:00');
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (8,'Neto Las Vigas','T','19.636900','-97.098594',NULL,'2826880645','L-D 07:00 a 22:00',NULL,NULL);
INSERT or IGNORE INTO servicios (id_servicio,descripcion,tipo,latitud,longitud,contacto_nombre,contacto_telefono,horario,created_at,updated_at) VALUES (9,'COMIDA PRUEBA','C','19.636900','-97.098594',NULL,'2826880645','L-D 07:00 a 22:00',NULL,NULL);
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,1,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,2,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,3,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,4,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,5,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,6,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,7,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,8,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,9,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,10,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,11,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,12,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,13,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,14,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,15,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,16,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,17,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,18,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,19,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,20,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,21,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,22,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,23,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,24,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,25,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,26,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,27,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,28,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,29,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,30,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,31,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,32,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,33,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,34,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,35,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,36,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,37,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,38,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,39,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,40,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,41,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,42,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,43,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,44,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,45,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,46,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,47,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,48,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,49,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,50,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,51,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,52,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,53,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,54,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,55,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,56,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,57,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,58,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,59,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,60,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,61,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,62,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,63,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,64,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,65,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,66,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,67,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,68,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,69,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (1,70,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,70,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,71,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,72,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,73,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,74,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,75,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,76,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,77,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,78,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,79,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,80,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,81,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,82,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,83,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,84,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,85,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,86,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,87,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,88,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,89,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,90,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,91,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,92,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,93,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,94,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,95,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,96,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,97,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,98,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,99,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,100,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,101,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,102,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,103,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,104,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,105,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,106,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,107,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,108,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,109,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,110,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,111,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,112,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,113,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,114,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,115,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,116,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,117,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,118,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,119,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,120,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,121,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,122,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,123,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,124,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,125,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,126,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,127,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,128,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,129,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,130,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,131,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,132,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,133,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,134,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,135,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,136,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,137,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO puntos_rutas (id_ruta,id_punto_control,created_at,updated_at) VALUES (2,1,'2020-06-30 11:42:00','2020-06-30 11:42:00');
INSERT or IGNORE INTO rutas (id_ruta,descripcion,dificultad,distancia,color,created_at,updated_at) VALUES (1,'El pequeño Dragón (Ascenso)','Alta',22000,'#FF0000','2020-06-30 11:36:00','2020-06-30 11:36:00');
INSERT or IGNORE INTO rutas (id_ruta,descripcion,dificultad,distancia,color,created_at,updated_at) VALUES (2,'El pequeño Dragón (Descenso)','Media',23000,'#64FF00','2020-06-30 11:36:00','2020-06-30 11:36:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (1,'19.621335','-97.105583','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (2,'19.622216','-97.105751','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (3,'19.623521','-97.105374','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (4,'19.624007','-97.105544','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (5,'19.624664','-97.105386','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (6,'19.624459','-97.105938','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (7,'19.624833','-97.105949','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (8,'19.625552','-97.105996','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (9,'19.626022','-97.105719','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (10,'19.626251','-97.10547','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (11,'19.626673','-97.105287','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (12,'19.62697','-97.105127','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (13,'19.627097','-97.105242','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (14,'19.627103','-97.105321','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (15,'19.626983','-97.105397','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (16,'19.626712','-97.105605','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (17,'19.626528','-97.105714','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (18,'19.626405','-97.106152','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (19,'19.626473','-97.106348','I','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (20,'19.626395','-97.106517','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (21,'19.626845','-97.107081','I','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (22,'19.627078','-97.107481','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (23,'19.62735','-97.10782','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (24,'19.627903','-97.108318','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (25,'19.628784','-97.108612','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (26,'19.6293','-97.108789','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (27,'19.629643','-97.109549','I','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (28,'19.629705','-97.109802','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (29,'19.629659','-97.110334','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (30,'19.629686','-97.110911','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (31,'19.629651','-97.110959','I','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (32,'19.629615','-97.111132','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (33,'19.629671','-97.111969','I','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (34,'19.629496','-97.112304','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (35,'19.627536','-97.114132','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (36,'19.627286','-97.114356','D','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (37,'19.625998','-97.115104','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (38,'19.624931','-97.115044','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (39,'19.624488','-97.115608','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (40,'19.623943','-97.116555','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (41,'19.623243','-97.119852','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (42,'19.62277','-97.120174','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (43,'19.622511','-97.120188','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (44,'19.621418','-97.121651','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (45,'19.620226','-97.122408','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (46,'19.619452','-97.123079','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (47,'19.616514','-97.127129','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (48,'19.610774','-97.131831','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (49,'19.608135','-97.130637','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (50,'19.60814','-97.130617','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (51,'19.608269','-97.12997','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (52,'19.608267','-97.129968','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (53,'19.604828','-97.129637','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (54,'19.603564','-97.129902','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (55,'19.601818','-97.130651','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (56,'19.60021','-97.131062','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (57,'19.598885','-97.131524','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (58,'19.597676','-97.131989','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (59,'19.596199','-97.132686','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (60,'19.595851','-97.133635','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (61,'19.595398','-97.133756','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (62,'19.59248','-97.135045','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (63,'19.592232','-97.135104','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (64,'19.591933','-97.13541','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (65,'19.589276','-97.134074','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (66,'19.589139','-97.133744','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (67,'19.589394','-97.132897','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (68,'19.589782','-97.132491','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (69,'19.590275','-97.131842','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (70,'19.588014','-97.131595','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (71,'19.587645','-97.131449','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (72,'19.586931','-97.131116','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (73,'19.585458','-97.131527','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (74,'19.584546','-97.131618','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (75,'19.583369','-97.131952','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (76,'19.582845','-97.132272','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (77,'19.581626','-97.131953','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (78,'19.579282','-97.131398','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (79,'19.57866','-97.131384','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (80,'19.577746','-97.130855','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (81,'19.577088','-97.130892','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (82,'19.575955','-97.130785','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (83,'19.575332','-97.129945','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (84,'19.573165','-97.127102','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (85,'19.57197','-97.126049','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (86,'19.570586','-97.123731','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (87,'19.570628','-97.121772','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (88,'19.570677','-97.120113','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (89,'19.569683','-97.119278','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (90,'19.570385','-97.118571','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (91,'19.570855','-97.117494','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (92,'19.574347','-97.108469','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (93,'19.573799','-97.106947','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (94,'19.572653','-97.103684','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (95,'19.57173','-97.098533','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (96,'19.573638','-97.099559','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (97,'19.573929','-97.098797','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (98,'19.573359','-97.097576','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (99,'19.573733','-97.097136','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (100,'19.578225','-97.099951','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (101,'19.57898','-97.09839','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (102,'19.579602','-97.098652','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (103,'19.58194','-97.100774','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (104,'19.584739','-97.102625','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (105,'19.587537','-97.100853','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (106,'19.587709','-97.100889','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (107,'19.589626','-97.102272','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (108,'19.588812','-97.104978','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (109,'19.594158','-97.110779','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (110,'19.594277','-97.112797','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (111,'19.594628','-97.113753','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (112,'19.595043','-97.113987','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (113,'19.596187','-97.114953','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (114,'19.59907','-97.112923','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (115,'19.601362','-97.113357','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (116,'19.602555','-97.112572','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (117,'19.602705','-97.113129','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (118,'19.603847','-97.113864','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (119,'19.60518','-97.114425','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (120,'19.608166','-97.115378','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (121,'19.609579','-97.115269','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (122,'19.61287','-97.114655','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (123,'19.613247','-97.114617','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (124,'19.613498','-97.11434','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (125,'19.613385','-97.113724','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (126,'19.61326','-97.11313','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (127,'19.615124','-97.111266','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (128,'19.615161','-97.111166','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (129,'19.615224','-97.110854','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (130,'19.61493','-97.110289','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (131,'19.614152','-97.109931','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (132,'19.614081','-97.109731','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (133,'19.615685','-97.109323','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (134,'19.61663','-97.109054','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (135,'19.617718','-97.109185','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (136,'19.620279','-97.108745','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO puntos_control (id_punto_control,latitud,longitud,direccion,altura,created_at,updated_at) VALUES (137,'19.621886','-97.106936','','','2020-06-29 13:23:00','2020-06-29 13:23:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (1,'Pino colorado (Pinus patula)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (2,'Ocote (Pinus teocote)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (3,'Pino blanco (Pinus pseudostrobus)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (4,'Pino chamaite (Pinus montezumae)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (5,'Pino acalocote (Pinus ayacahuite)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (6,'Ilite (Alnus acaminata)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (7,'Encino (Quercus rugosa)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (8,'Escobillo (Baccharis conferta)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (9,'Capulín (Prunus serotina)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (10,'Madroño (Arbutus xalapensis)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (11,'Matamoscas (Amanita muscaria)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (12,'Yema de huevo (A. caesarea)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (13,'Oronja vinosa (A. rubescens)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (14,'Pipa de indio (Monotropa coccinea)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (15,'Hongo blanco (Tricholoma magnivelare)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (16,'Comototolcozatl (Rhodophyllus abortivus)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (17,'Hongo clavito (Lyophyllum decastes)','F',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (18,'Lagartija escamosa de mezquite (Sceloporus grammicus)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (19,'Lagartija espinosa o techachaco (S. mucronatus)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (20,'Lagartija escorpión (Barisia imbricata)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (21,'Camaleón (Phrynosoma orbiculare)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (22,'Culebra terrestre (Conopsis lineata)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (23,'Culebra de agua (Thamnophis scalaris)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (24,'Cascabel (Crotalus intermedius y C. triseriatus)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (25,'Rana arborícola (Hyla eximia)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (26,'Rana manchada (Lithobates spectabilis)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (27,'Ajolote (Ambystoma velasci)','A',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (28,'San Juan del Monte fue declarada como Área Verde Reservada para la Educación Ecológica mediante decreto publicado en la Gaceta Oficial del estado de Veracruz, el 30 de octubre de 1980. ','H',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (29,'San Juan del Monte se localiza en la zona montañosa del centro de Veracruz en la vertiente del Cofre de Perote dentro del municipio de Las Vigas de Ramírez. ','H',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO informacion (id_informacion,descripcion,tipo,ruta_imagen,created_at,updated_at) VALUES (30,'Debido a que en 1995 y 1998 se registraron incendios forestales en la región que afectaron parte del bosque de San Juan del Monte, en la actualidad se mantiene un estricto control en la apertura, mantenimiento y limpieza de brechas contra incendios.','H',NULL,'2020-06-29 20:29:00','2020-06-29 20:29:00');
INSERT or IGNORE INTO usuarios (id_usuario,nombre,apellidos,correo,contrasenia,sexo,ultima_fecha_ingreso,clave,created_at,updated_at) VALUES (1,'administrador',NULL,'correo@correo.com','administrador','H','2020-06-29 12:59:00',NULL,'2020-06-29 12:59:00','2020-06-29 12:59:00');